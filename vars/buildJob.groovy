def call(){
    sh "sudo docker login -u ${env.DOCKER_USER} -p ${env.DOCKER_PASSWORD} ${env.REGISTRY_URL}"
    sh "sudo docker build -t ${env.DOCKER_USER}/${env.APP_IMAGE}:${env.APP_TAG} -f docker/vm/Dockerfile ."
    sh "sudo docker build -t ${env.DOCKER_USER}/${env.LB_IMAGE}:${env.LB_TAG}  docker/lb"
}