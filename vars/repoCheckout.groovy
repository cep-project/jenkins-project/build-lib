def call(Map config){
    def scmVars = checkout([
        $class: 'GitSCM',
        branches: [[name: config.branch]],
        userRemoteConfigs: [
            [ url: config.repo_url ]
        ]
    ])
}